<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Customer;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        //$id = Auth::id();
        //$customers = Customer::all();
        //$uid = Customer::user_id();
        //$id= Customer(user_id);
        //$users = User::all();
        //$users = User::all();
        //get all todos
        //$user = User::find($id);
        //$tasks = $user->tasks;
        $customers = Customer::all();
       // $id=Customer::all();
        //$user = User::find($id->user_id);
        return view('customers.index', compact('customers'),compact('user'));
         //$tasks = Task::all();
      // return view('tasks.index', ['tasks' => $tasks]);
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required',
            'phone'=>'required']);
     
            
            $customer= new Customer();
            $id=Auth::id();
            $customer->name = $request->name;
            $customer->email = $request->email;
            $customer->phone = $request->phone;
            $customer->user_id=$id;
            $customer->save();
            return redirect('customers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customers = Customer::find($id);
        return view('customers.edit',compact('customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customers = Customer::findOrFail($id);            
        $customers->update($request->except(['_token'])); 
        return redirect('customers'); 

    }

    public function done($id)
    {
        //only if this todo belongs to user 
        if (Gate::denies('manager')) {
            abort(403,"You are not allowed to mark tasks as dome..");
         }          
        $customer = Customer::findOrFail($id);            
        $customer->status = 1; 
        $customer->save();
        return redirect('customers');    
    }   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
       }
        
        $customer = Customer::find($id);
        $customer->delete();
        return redirect('customers');
    }
}
