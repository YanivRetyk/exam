<h1>Edit customers</h1>
<form method = 'post' action="{{action('CustomerController@update', $customers->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "title">customers to Update:</label>
    <input type= "text" class = "form-control" name= "name" value = "{{$customers->name}}">
    <input type= "email" class = "form-control" name= "email" value = "{{$customers->email}}">
    <input type= "int" class = "form-control" name= "phone" value = "{{$customers->phone}}">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="edit">
</div>

</form>
